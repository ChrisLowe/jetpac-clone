
Jetpack clone

By Christopher Lowe
Assignment 2  for Cert IV C++, Semester 2 2011



--------------
Introduction

   This is a partial clone of the classic 1993 game Jetpack written in
   C++ and Direct3D.  It's purpose is to demonstrate an understanding of
   	- Object Orientated programming
   	- Inheritance, Encapsulation and Polymorphism
   	- Use of external libraries (Direct3D, DirectInput and XAudio2)
   	- Sprite animation and rotation and
   	- Linked lists
 

--------------
Controls

The objective of the game is to collect all the coins and avoid the enemies.
When all the coins are collected you can exit through the door.

   Left arrow	-> Move Left
   Right arrow  -> Move Right
   Space	-> Thrust
   F1		-> Reset
   ESC 		-> Exit


