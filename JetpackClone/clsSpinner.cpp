/*
 *
 *      CertIV Semester2 2011 C++ Assignment 2   -  Jetpack clone
 *
 *
 *   
 *      Author: Christopher Lowe
 *      Copywrite (C) 2011 Christopher Lowe,  lowey2002@gmail.com
  *
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * 
 */

#include "StdAfx.h"
#include "clsSpinner.h"
#include "util.h"


clsSpinner::clsSpinner(void)
{
	moveRight = true;
	moveUp = true;
	moveSpeed = 1.0f;
	rotation=3.14f;
	isRotate = true;
}


clsSpinner::~clsSpinner(void)
{
}

void clsSpinner::rotate(LPD3DXSPRITE spt)
{
	// Spinners rotate around their center
	D3DXVECTOR2 spriteCentre=D3DXVECTOR2(xPos + (width / 2),yPos + (height / 2));

	rotation+=0.1f;

	// Rotation matrix
	D3DXMATRIX mat;
	D3DXMatrixTransformation2D(&mat,NULL,0.0,NULL,&spriteCentre,rotation,NULL);

	// Apply the matrix to the sprite
	spt->SetTransform(&mat);


}


void clsSpinner::move(clsObject *pObj)
{

	int col = directCollision(pObj);
	if (col == ROOF_COLLISION) moveUp = false;
	if (col == FLOOR_COLLISION) moveUp = true;
	if (col == RIGHT_COLLISION) moveRight = false;
	if (col == LEFT_COLLISION) moveRight = true;

	if (moveUp) 
		yPos -= moveSpeed;
	else
		yPos += moveSpeed;

	if (moveRight)
		xPos += moveSpeed;
	else
		xPos -= moveSpeed;
}
