/*
 *
 *      CertIV Semester2 2011 C++ Assignment 2   -  Jetpack clone
 *
 *
 *   
 *      Author: Christopher Lowe
 *      Copywrite (C) 2011 Christopher Lowe,  lowey2002@gmail.com
  *
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  
 */

#pragma once

#include "resource.h"
#include <windows.h>
#include <windowsx.h>
#include <time.h>
#include <cstdlib> 
#include <iostream>
#include <d3d9.h>
#include <d3dx9.h>
#define DIRECTINPUT_VERSION 0x0800  //Force version
#include <dinput.h>
#include <XAudio2.h>
#include "util.h"
#include "clsAbstractSprite.h"
#include "clsObject.h"
#include "clsPlayer.h"
#include "clsSpinner.h"
#include "levels.h"
#include "clsDoor.h"


// DirectX library
#pragma comment (lib, "d3d9.lib")
#pragma comment (lib, "d3dx9.lib")
#pragma comment (lib, "dinput8.lib")
#pragma comment (lib, "dxguid.lib")


// Screen resolution
#define SCREEN_WIDTH  800
#define SCREEN_HEIGHT 600


// Keyboard hooks
#define KEY_DOWN(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000) ? 1 : 0)
#define KEY_UP(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000) ? 0 : 1)


// Windowed mode
const bool WINDOWED_MODE = true;


// Title
const LPCWSTR TITLE = L"Jetpack Clone by Christopher Lowe";


// The timeout for the game over screen
const float GAME_OVER_DELAY = 15.0f;
float gameOverDelay = 0.0f;

// Is the game over?
bool gameOver = false;



// Game creation
void loadGame(void);
void startLevel(int level);
void createEnemies(int level);
//void createTerrainObject(int, int, int);

// Game logic prototypes
void gameLoop(void);
void gameDraw(void);
void restartGame(void);
void deleteAllObjects(void);

// Cleanup prototypes
void cleanD3D(void); 
void cleanDInput(void);



// The arrow key being pressed
bool moveLeft = false;
bool moveRight = false;
bool thrust = false;

// DirectX prototypes
void initD3D(HWND hWnd); 
void initDInput(HINSTANCE hInstance, HWND hWnd);  
void render_frame(void); 
void detect_input(HWND hWnd);
void initSound(void);

// Window Message Handler prototypes
LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

// The current mouse co-ordinates and mouse click masks
LONG mouseX;
LONG mouseY;
BYTE mouseClick[4];
const BYTE LEFT_MOUSE_BUTTON = 0;
const BYTE RIGHT_MOUSE_BUTTON = 1;
LONG mouseDeltaX;
LONG mouseDeltaY;

// DirectX pointers
LPDIRECT3D9 d3d;					// the pointer to our Direct3D interface
LPDIRECT3DDEVICE9 d3ddev;			// the pointer to the device class
LPD3DXSPRITE d3dspt;				// the pointer to our Direct3D Sprite interface
LPDIRECTINPUT8 din;					// the pointer to our DirectInput interface
LPDIRECTINPUTDEVICE8 dinmouse;		// the pointer to the mouse device
DIMOUSESTATE mousestate;			// the pointer to the mouse state
IXAudio2* pXAudio2;					// the pointer to the audio device

LPD3DXSPRITE d3dspt_enemy;		// a different pointer to a Direct3D Sprite interface for rotating sprites
LPD3DXSPRITE d3dspt_player;		// a different pointer to a Direct3D sprite for player mirroring