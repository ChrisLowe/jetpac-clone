/*
 *      clsAbstractSprite.cpp
 *
 *      Source file for the abstract sprite.
 *
 *			- Holds the xPos & yPos, and the pictures width and height
 *
 *
 *      As part of CertIV Semester2 2011 Assignment 2   -  Jetpack clone
 *
 *
 *   
 *      Author: Christopher Lowe
 *      Copywrite (C) 2011 Christopher Lowe,  lowey2002@gmail.com
 *       *
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * 
 */

#include "StdAfx.h"
#include "clsAbstractSprite.h"


clsAbstractSprite::clsAbstractSprite(void)
{
	currentFrame = 0;
	frameCount = 0;
	frameDelay = 10.0f;
	mirrored = false;
}


clsAbstractSprite::~clsAbstractSprite(void)
{
	sprite->Release();
}

void clsAbstractSprite::changeLocation(float newX, float newY) {
	xPos = newX;
	yPos = newY;
}

float clsAbstractSprite::getXPos() {
	return xPos;
}

float clsAbstractSprite::getYPos() {
	return yPos;
}

float clsAbstractSprite::getHeight() {
	return height;
}

float clsAbstractSprite::getWidth() {
	return width;
}


// Create the sprite
void clsAbstractSprite::createPicture(LPDIRECT3DDEVICE9 dev, LPWSTR filename, float startx, float starty, float mwidth, float mheight) 
{

	xPos = startx;
	yPos = starty;
	width = mwidth;
	height = mheight;

	//Transparency
	D3DXCreateTextureFromFileEx(dev,    // the device pointer
                             filename,    // the file name
                             D3DX_DEFAULT,    // default width
                             D3DX_DEFAULT,    // default height
                             D3DX_DEFAULT,    // no mip mapping
                             NULL,				// regular usage
                             D3DFMT_A8R8G8B8,    // 32-bit pixels with alpha
                             D3DPOOL_MANAGED,    // typical memory handling
                             D3DX_DEFAULT,    // no filtering
                             D3DX_DEFAULT,    // no mip filtering
                             D3DCOLOR_XRGB(255, 0, 255),		// the transparent colour key  - Pink ~~
                             NULL,    // no image info struct
                             NULL,    // not using 256 colors
                             &sprite);    // load to sprite
}




void clsAbstractSprite::setFrame(int frameX, int frameY) {

	int fx0 = (frameX * ( (int)width - 1)) + (frameX * 2); //left
	int fx1 = fx0 + (int) width; //right
	int fy0 = (frameY * ( (int)height - 1)) + (frameY * 2); //top
	int fy1 = fy0 + (int) height; //bottom

	SetRect(&frame, fx0, fy0, fx1, fy1);
}


void clsAbstractSprite::createFrame(int numberOfFrames, int tilesetX, int tilesetY, float frameWidth, float frameHeight){
	frameCount = numberOfFrames;
	width = frameWidth;
	height = frameHeight;
	
	setFrame(0, 0);
}



// Draw the sprite
void clsAbstractSprite::drawPicture(LPD3DXSPRITE spt) 
{
	D3DXVECTOR3 position(xPos, yPos, 0);
	D3DXVECTOR3 center(0, 0, 0);
	spt->Draw(sprite, &frame, &center, &position, D3DCOLOR_ARGB(255, 255, 255, 255));

}