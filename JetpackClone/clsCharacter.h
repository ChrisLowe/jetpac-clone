/*
 *
 *      CertIV Semester2 2011 C++ Assignment 2   -  Jetpack clone
 *
 *
 *   
 *      Author: Christopher Lowe
 *      Copywrite (C) 2011 Christopher Lowe,  lowey2002@gmail.com
 *      
 *
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * 
 */

#pragma once
#include "clsabstractsprite.h"
#include "clsObject.h"
#include "util.h"

const int ROOF_COLLISION = 1;
const int FLOOR_COLLISION = 2;
const int RIGHT_COLLISION = 3;
const int LEFT_COLLISION = 4;
const int DIRECT_COLLISION = 5;
const int COLLISION = 6;

class clsCharacter :
	public clsObject
{
public:
	clsCharacter(void);
	~clsCharacter(void);
	virtual bool simpleCollision(clsObject *pObj);
	virtual collision_t* collision(clsObject *pObj);
	int directCollision(clsObject *pObj);
};

