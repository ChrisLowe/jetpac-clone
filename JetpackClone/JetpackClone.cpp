/*
 *
 *      CertIV Semester2 2011 C++ Assignment 2   -  Jetpack clone
 *   
 *      Author: Christopher Lowe
 *      Copywrite (C) 2011 Christopher Lowe,  lowey2002@gmail.com
 *
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "stdafx.h"
#include "JetpackClone.h"


// The player object
clsPlayer *pPlayer;

// The terrain objects
clsObject *pStaticTerrain;

// The coin objects
clsCoin *pMyCoins;
int coinsRemaining = 0;

// The door
clsDoor *pDoor;

// The enemy objects
clsEnemy *pEnemies;

// The game over screen
clsAbstractSprite *pGameOver;


// Load the game resources
void loadGame(void) {

	// Clear the lists, enables resetting
	resetLevelObjects();
	pStaticTerrain = NULL;
	pEnemies = NULL;
	pMyCoins = NULL;
	pDoor = NULL;

	// Load all the assets (level.cpp does all the work)
	pStaticTerrain = loadLevel(d3ddev);
	pEnemies = loadEnemies(d3ddev);
	pMyCoins = loadCoins(d3ddev);
	pDoor = loadDoor(d3ddev, 26, 17);

	// Load the player
	pPlayer = new clsPlayer();
	pPlayer->createPicture(d3ddev, L"player.png", 500, 30, 32, 32);  //500, 100
	pPlayer->createFrame(6, 6, 1, 32, 32);
	pPlayer->setCollectSound(pXAudio2, L"collect.wav");
	//pPlayer->setThrustSound(pXAudio2, L"thrust.wav");

	// Load the game over screen
	pGameOver = new clsAbstractSprite();
	pGameOver->createPicture(d3ddev, L"gameover.png", 0 ,0 ,256, 256);
	pGameOver->createFrame(1, 0, 0, 256, 256);
	pGameOver->changeLocation(0.5 * (SCREEN_WIDTH - 256), 0.5 * (SCREEN_HEIGHT - 256));

}



void gameDraw(void) {

	// Draw the player
	pPlayer->drawPicture(d3dspt_player);

	// Draw the door
	pDoor->drawPicture(d3dspt);

	// Draw the coins
	clsCoin *pTempCoin = pMyCoins;
	coinsRemaining = 0;
	while (pTempCoin != NULL) {
		coinsRemaining++;
		pTempCoin->drawPicture(d3dspt);
		pTempCoin->animateSpin();
		pTempCoin = pTempCoin->pNext;
	}

	// Open the door if all the coins are collected 
	if (coinsRemaining == 1) {   // this should be in gameLoop()
		pDoor->animateDoorOpen();   // but I can take advantage of the above loop this way.
	}

	// Draw the enemies
	clsEnemy *pTempEnemy = pEnemies;
	while (pTempEnemy != NULL) {
		if (pTempEnemy->isRotate) {
			pTempEnemy->rotate(d3dspt_enemy);   //draw rotating enemies on their own lpdxsprite
			pTempEnemy->drawPicture(d3dspt_enemy);
		} else {
			pTempEnemy->drawPicture(d3dspt);
		}
		pTempEnemy = pTempEnemy->pNext; 
	}


	// Draw the terrain
	clsObject *pTemp = pStaticTerrain;
	while (pTemp != NULL)
	{
		pTemp->drawPicture(d3dspt);
		pTemp = pTemp->pNext;
	}

	// Draw the game over screen
	if (gameOver)
		pGameOver->drawPicture(d3dspt);


}



// Move the game objects and handle the play logic
void gameLoop(void) {

	// Move the enemies
	clsEnemy *pTempEnemy = pEnemies;
	while (pTempEnemy != NULL) {
		pTempEnemy->move(pStaticTerrain);
		
		// Check if the enemy collides with a player. Kills the player.
		if (pTempEnemy->simpleCollision(pPlayer)) {
			pPlayer->die();
			gameOver = true;
		}

		pTempEnemy = pTempEnemy->pNext;
	}

	// Check if the player collects a coin (also deletes the coins and plays the sound)
	pMyCoins = removeCoinFromListOnContact(pPlayer, pXAudio2);

	// Check if the player collides with the door (once it is open)
	if (pPlayer->simpleCollision(pDoor) && (coinsRemaining == 1)) {
		gameOver = true;
	}

	// Check if the player collides with terrain objects. collision_t see util.h
	collision_t *pPlayerCollision = pPlayer->collision(pStaticTerrain);

	if (pPlayer->isDead) {
		if (!(pPlayerCollision->down))
			pPlayer->moveDown();  // Stop floating corpses

	} else {
		// Check if the player has fallen of left of screen
		if (pPlayer->getXPos() <= 16.0f) 
			pPlayer->changeLocation(16.0f, pPlayer->getYPos());
	
		// Check if the player has fallen of right of screen
		if (pPlayer->getXPos() >= (SCREEN_WIDTH - pPlayer->getWidth() - 16.0f))
			pPlayer->changeLocation(SCREEN_WIDTH - pPlayer->getWidth() - 16.0f, pPlayer->getYPos());

		// Check if the player has fallen of top of screen
		if (pPlayer->getYPos() <= 16.0f)
			pPlayer->changeLocation(pPlayer->getXPos(), 16.0f);

		// Check if the player has fallen of the bottom of screen
		if (pPlayer->getYPos() >= SCREEN_HEIGHT - (pPlayer->getHeight() + 40.0f))
			pPlayer->changeLocation(pPlayer->getXPos(), (SCREEN_HEIGHT - (pPlayer->getHeight() + 40.0f)));

		//
		//POSTCONDITIONS: The player is in the screen area.

		// Only allow the player to move if there is no collision in that direction.
		if (thrust) {
			if (pPlayerCollision->up) {
				pPlayer->cannotMoveUp(moveLeft); // Player is thrusting with something above them
			}
			else
				pPlayer->moveUp(moveLeft);
		} else if (!(pPlayerCollision->down)) {
			pPlayer->moveDown();
		}

		if (!(pPlayerCollision->left)) {
			if (moveLeft) pPlayer->moveLeft();
		}

		if (!(pPlayerCollision->right)) {
			if (moveRight) pPlayer->moveRight();
		}


	} //!(pPlayer->isDead())
}


void deleteAllObjects(void)
{
	// Delete terrain
	clsObject *pTemp = pStaticTerrain;
	while (pTemp != NULL) {
		clsObject *pNext = pTemp->pNext;
		delete pTemp;
		pTemp = pNext;
	}

	// Delete enemies
	clsEnemy *pTemp1 = pEnemies;
	while (pTemp1 != NULL) {
		clsEnemy *pNext = pTemp1->pNext;
		delete pTemp1;
		pTemp1 = pNext;
	}

	// Delete coins
	clsCoin *pTemp2 = pMyCoins;
	while (pTemp2 != NULL) {
		clsCoin *pNext = pTemp2->pNext;
		delete pTemp2;
		pTemp2 = pNext;
	}
	
	// Delete the door
	delete pDoor;
	pDoor = NULL;

}

void restartGame(void) {
	gameOver = false;
	deleteAllObjects();
	loadGame();
}


void leftKeyDown(void) {
	if (pPlayer->isDead == false)  {
		moveLeft = true;
		pPlayer->mirrored = true;
	}
}

void leftKeyUp(void) {
	if (pPlayer->isDead == false) 
		moveLeft = false;
}

void rightKeyDown(void) {
	if (pPlayer->isDead == false) {
		moveRight = true;
		pPlayer->mirrored = false;
	}
}

void rightKeyUp(void) {
	if (pPlayer->isDead == false) 
		moveRight = false;
}

void spaceKeyDown(void) {
	if (pPlayer->isDead == false)  {
		thrust = true;
		pPlayer->startThrust(moveLeft);
	}
}

void spaceKeyUp(void) {
	if (pPlayer->isDead == false)  {
		thrust = false;
		if ((!moveRight) && (!moveLeft))
			pPlayer->endThrust(moveLeft);
	}
}


//
//
// Below here is the engine
//
//



// The Entry point
int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPSTR lpCmdLine,
                   int nCmdShow)
{
	// Create and register a window
    HWND hWnd;
    WNDCLASSEX wc;

    ZeroMemory(&wc, sizeof(WNDCLASSEX));
    wc.cbSize = sizeof(WNDCLASSEX);
    wc.style = CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc = (WNDPROC)WindowProc;
    wc.hInstance = hInstance;
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.lpszClassName = L"WindowClass1";

    RegisterClassEx(&wc);

    hWnd = CreateWindowEx(NULL,
                          L"WindowClass1",
                          TITLE,
                          WS_OVERLAPPEDWINDOW,
                          0, 0,
                          SCREEN_WIDTH, SCREEN_HEIGHT,
                          NULL,
                          NULL,
                          hInstance,
                          NULL);

    ShowWindow(hWnd, nCmdShow);

    // Initialize Direct3D
    initD3D(hWnd);
	
	// Initialize DirectInput
	initDInput(hInstance, hWnd);    

	// Initialize Sound
	initSound();


    // Main loop
    MSG msg;
    while(TRUE)
    {
        DWORD starting_point = GetTickCount();

        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT)
                break;

            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }

		detect_input(hWnd);
        render_frame();

		// Game loop
		gameLoop();

        // Check for user input
        if(KEY_DOWN(VK_ESCAPE))
            PostMessage(hWnd, WM_DESTROY, 0, 0);

		if (KEY_DOWN(VK_LEFT)) {
			leftKeyDown();
		}
		
		if (KEY_DOWN(VK_RIGHT)) {
			rightKeyDown();
		} 
		
		if (KEY_DOWN(VK_SPACE)) {
			//pPlayer->playThrustSound(pXAudio2);   .. buggy :(
			spaceKeyDown();
		}

		if (KEY_UP(VK_SPACE)) {
			spaceKeyUp();
		}

		if (KEY_UP(VK_RIGHT)) {
			rightKeyUp();
		}

		if (KEY_UP(VK_LEFT)) {
			leftKeyUp();
		}
		
		if (KEY_DOWN(VK_F1)) {
			restartGame();
		}


		// Limit framerate
        while ((GetTickCount() - starting_point) < 25);
    }


    // clean up DirectX and COM
    cleanD3D();
	cleanDInput();

    return msg.wParam;
}



// Initializes DirectInput
void initDInput(HINSTANCE hInstance, HWND hWnd)
{
    // Create the DirectInput interface
    DirectInput8Create(hInstance,    
                       DIRECTINPUT_VERSION,    
                       IID_IDirectInput8,  
                       (void**)&din,    
                       NULL); 

	// Create the mouse device interface
    din->CreateDevice(GUID_SysMouse,
                      &dinmouse,
                      NULL);

    // Set the mouse interface format
    dinmouse->SetDataFormat(&c_dfDIMouse);

    // Set the mouse interface control
    dinmouse->SetCooperativeLevel(hWnd, DISCL_EXCLUSIVE | DISCL_FOREGROUND);
}



// Updates the latest mouse input events
void detect_input(HWND hWnd)
{
	// Create storage for the mouse-states
	static DIMOUSESTATE mousestate;    
    dinmouse->Acquire();

	// Mouse click state
	mouseClick[0] = mousestate.rgbButtons[0];
	mouseClick[1] = mousestate.rgbButtons[1];
	mouseClick[2] = mousestate.rgbButtons[2];
	mouseClick[3] = mousestate.rgbButtons[3];

	// Mouse delta
	mouseDeltaX = mousestate.lX;
	mouseDeltaY = mousestate.lY;

	// Relative mouse position
    POINT cursorPos;
    GetCursorPos(&cursorPos);
    mouseX = cursorPos.x;
    mouseY = cursorPos.y;
	

	// Right Mouse Button
	if (mouseClick[0] & 0x80) {
		
	}
    dinmouse->GetDeviceState(sizeof(DIMOUSESTATE), (LPVOID)&mousestate);
}




// Initializes Direct3D for use
void initD3D(HWND hWnd)
{
	// I don't understand any of this...
    d3d = Direct3DCreate9(D3D_SDK_VERSION);
    D3DPRESENT_PARAMETERS d3dpp;

    ZeroMemory(&d3dpp, sizeof(d3dpp));
	d3dpp.Windowed = WINDOWED_MODE;
    d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
    d3dpp.hDeviceWindow = hWnd;
    d3dpp.BackBufferFormat = D3DFMT_X8R8G8B8;
    d3dpp.BackBufferWidth = SCREEN_WIDTH;
    d3dpp.BackBufferHeight = SCREEN_HEIGHT;
	
	d3d->CreateDevice(D3DADAPTER_DEFAULT,
                      D3DDEVTYPE_HAL,
                      hWnd,
                      D3DCREATE_SOFTWARE_VERTEXPROCESSING,
                      &d3dpp,
                      &d3ddev);

    D3DXCreateSprite(d3ddev, &d3dspt);
	D3DXCreateSprite(d3ddev, &d3dspt_enemy);
	D3DXCreateSprite(d3ddev, &d3dspt_player);

	// Load game
	loadGame();

    return;
}

// Initializes the XAudio2 device
void initSound()
{
	CoInitializeEx( NULL, COINIT_MULTITHREADED );
	pXAudio2 = NULL;
	XAudio2Create( &pXAudio2, 0, XAUDIO2_DEFAULT_PROCESSOR );
	IXAudio2MasteringVoice* pMasterVoice = NULL;

	pXAudio2->CreateMasteringVoice( &pMasterVoice, XAUDIO2_DEFAULT_CHANNELS,
                            XAUDIO2_DEFAULT_SAMPLERATE, 0, 0, NULL );
}



// Render a single frame
void render_frame(void)
{
    //Begin the frame
    d3ddev->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);
    d3ddev->BeginScene(); 
    d3dspt->Begin(D3DXSPRITE_ALPHABLEND);   
	d3dspt_enemy->Begin(D3DXSPRITE_ALPHABLEND);   
	d3dspt_player->Begin(D3DXSPRITE_ALPHABLEND);

	// Draw the game
	gameDraw();


	//Clean up the frame
    d3dspt->End();    
	d3dspt_enemy->End();  
	d3dspt_player->End();
    d3ddev->EndScene();  
    d3ddev->Present(NULL, NULL, NULL, NULL);
}




// Clean Direct3D
void cleanD3D(void)
{
    d3ddev->Release();
    d3d->Release();
	dinmouse->Unacquire();
	pXAudio2->Release();
}

// Clean DirectInput
void cleanDInput(void)
{
    dinmouse->Unacquire();    // make sure the mouse in unacquired
    din->Release();    // close DirectInput before exiting
}


// Window Message Handler
LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch(message)
    {
        case WM_DESTROY:
            {
                PostQuitMessage(0);
                return 0;
            } break;
    }

    return DefWindowProc (hWnd, message, wParam, lParam);
}

