/*
 *      clsAbstractSprite.h
 *
 *      Header file for the abstract sprite.
 *		Holds the xPos, yPox, width and height and implements the getters/setters for these
 *
 *
 *      As part of CertIV Semester2 2011 Assignment 2 - Jetpack clone
 *
 *
 *   
 *      Author: Christopher Lowe
 *      Copywrite (C) 2011 Christopher Lowe,  lowey2002@gmail.com
 *
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * 
 */


#pragma once

#include <d3d9.h>
#include <d3dx9.h>
#include <XAudio2.h>
#include "playSound.h"


// The Direct3D Library file
#pragma comment (lib, "d3d9.lib")
#pragma comment (lib, "d3dx9.lib")

class clsAbstractSprite
{
public:
	clsAbstractSprite(void);
	~clsAbstractSprite(void);
	
	void changeLocation(float newX, float newY);
	float getXPos();
	float getYPos();
	float getWidth();
	float getHeight();
	void createFrame(int numberOfFrames, int tilesetX, int tilesetY, float frameWidth, float frameHeight);
	virtual void createPicture(LPDIRECT3DDEVICE9 dev, LPWSTR filename, float startXPos, float startYPos, float mwidth, float mheight);
	void setFrame(int framex, int framey);
	virtual void drawPicture(LPD3DXSPRITE spt);

	bool mirrored;

protected:
	LPDIRECT3DTEXTURE9 sprite;		// the pointer to the sprite
	float width, height;
	float xPos, yPos;
	RECT frame;
	int currentFrame;
	int frameCount;
	int frameX;
	int frameY;
	float frameDelay;
	int xFrames;
	int yFrames;
};
