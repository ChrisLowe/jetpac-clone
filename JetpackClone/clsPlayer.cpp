/*
 *
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * 
*/

#include "StdAfx.h"
#include "clsPlayer.h"


clsPlayer::clsPlayer(void)
{
	thrustOn = false;
	playerMoveDelay = 3;
	playerMoveSpeed = 3.0f;
	vUp = 0.0f;
	vSide = 0.0f;
	isDead = false;
	collectSound = 0.0f;
}


clsPlayer::~clsPlayer(void)
{

}


void clsPlayer::animateThrust(bool movingLeft)
{
	static int moveDelay = 0;
	moveDelay++;

	if (moveDelay > playerMoveDelay) {
			if (movingLeft)
				setFrame(3, 1);
			else
				setFrame(3, 0);
		if (moveDelay > playerMoveDelay * 2)
			if (movingLeft)
				setFrame(4, 1);
			else
				setFrame(4, 0);
		if (moveDelay > playerMoveDelay * 3)
			moveDelay = 0;
	} else {
		if (movingLeft)
			setFrame(3, 1);
		else
			setFrame(3, 0);
	}
}

void clsPlayer::animateWalk(bool movingLeft)
{
	static int moveDelay = 0;
	moveDelay++;
	if (moveDelay > playerMoveDelay) {
		if (movingLeft)
			setFrame(1, 1);
		else
			setFrame(1, 0);

		if (moveDelay > playerMoveDelay * 2)
			if (movingLeft)
				setFrame(2, 1);
			else
				setFrame(2, 0);
		
		if (moveDelay > playerMoveDelay * 3)
			if (movingLeft)
				setFrame(0, 1);
			else
				setFrame(0, 0);

		if (moveDelay > playerMoveDelay * 4) {
			if (movingLeft)
				setFrame(1, 1);
			else
				setFrame(1, 0);

			moveDelay = 0;
		}
	} else {
		if (movingLeft)
			setFrame(1, 1);
		else
			setFrame(1, 0);
	}
}

void clsPlayer::setMoveSpeed(float newMoveSpeed) {
	playerMoveSpeed = newMoveSpeed;
}

void clsPlayer::moveDown(void)
{
	//setFrame(0, 0);

	if (vUp <= MAX_UP_SPEED) {
		vUp += (MOVE_SPEED_INCREMENT * GRAVITY);
	}

	changeLocation(getXPos(), getYPos() + vUp);
}

void clsPlayer::moveUp(bool movingLeft)
{
	animateThrust(movingLeft);

	if (vUp >= MIN_UP_SPEED) {
		vUp -= MOVE_SPEED_INCREMENT;
	}

	changeLocation(getXPos(), getYPos() + vUp);
}


// Player is thrusting but cannot move up
void clsPlayer::cannotMoveUp(bool movingLeft)
{
	animateThrust(movingLeft);
	changeLocation(getXPos(), getYPos() + COLLISION_FIX_SPEED);
	vUp = 0.0f;
}


void clsPlayer::moveLeft(void)
{
	if (vSide > 0.0f) vSide = -0.1f;

	if (vSide >= MIN_SIDE_SPEED) {
		vSide -= MOVE_SPEED_INCREMENT;
	}
	
	changeLocation(getXPos() + vSide, getYPos());

	animateWalk(true);
	
}

void clsPlayer::moveRight(void)
{
	if (vSide < 0.0f) vSide = 0.1f;

	if (vSide <= MAX_SIDE_SPEED) {
		vSide += MOVE_SPEED_INCREMENT;
	}

	changeLocation(getXPos() + vSide, getYPos());
	animateWalk(false);
}


void clsPlayer::startThrust(bool movingLeft) {

	if (vUp > 0.0f) vUp = -0.1f;

	thrustOn = true;
	moveUp(movingLeft);
}

void clsPlayer::endThrust(bool movingLeft) {
	thrustOn = false;
	if (movingLeft)
		setFrame(0, 1);
	else
		setFrame(0, 0);
}

void clsPlayer::die(void) {
	isDead = true;
	setFrame(5, 0);
}


void clsPlayer::setCollectSound(IXAudio2 *pXAudio2, LPWSTR filename) 
{
	createSound(pXAudio2, &pSourceVoice, filename, &collectBuffer, &wfx);
}

void clsPlayer::playCollectSound(IXAudio2 *pXAudio2) {
		pXAudio2->CreateSourceVoice( &pSourceVoice, (WAVEFORMATEX*)&wfx,
					  0, XAUDIO2_DEFAULT_FREQ_RATIO, NULL, NULL, NULL );
		pSourceVoice->SubmitSourceBuffer( &collectBuffer );
		pSourceVoice->Start( 0, XAUDIO2_COMMIT_NOW );
}

			
