/*
 *
 *      CertIV Semester2 2011 C++ Assignment 2   -  Jetpack clone
 *
 *
 *   
 *      Author: Christopher Lowe
 *      Copywrite (C) 2011 Christopher Lowe,  lowey2002@gmail.com
  *
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * 
 */

#include "StdAfx.h"
#include "levels.h"

clsObject *pTerrain;
clsEnemy *pEnemy;
clsCoin *pCoins;


void resetLevelObjects(void)
{
	pTerrain = NULL;
	pEnemy = NULL;
	pCoins = NULL;
}


void addCoinToCoinList(clsCoin *pTemp)
{
	// Add the new enemy to the head of the enemy list
	pTemp->pNext = pCoins;
	pCoins = pTemp;
}


// Removes the coin from the list if a simpleCollision occurs.
// It returns a pointer to the updated linked list
clsCoin* removeCoinFromListOnContact(clsPlayer *pPlayer, IXAudio2* pXA2)
{
	clsCoin *pHead = pCoins;
	clsCoin *pCurrent = pHead->pNext;
	clsCoin *pPrevious = pHead;

	if (pHead!=NULL) {
		if (pPlayer->simpleCollision(pHead)) {
			pHead = pCurrent;
			delete pPrevious;
			return pHead;
		}

		while (pCurrent != NULL) {
			if (pPlayer->simpleCollision(pCurrent)) {
				pPrevious->pNext = pCurrent->pNext;

				//Play the collect coin sound
				pPlayer->playCollectSound(pXA2);

				delete pCurrent;
				return pHead;
			}
			pPrevious = pCurrent;
			pCurrent = pCurrent->pNext;
		}

	}

	return pHead;
}


void createCoinObject(int xGrid, int yGrid, LPDIRECT3DDEVICE9 d3ddev)
{
	float xPos = (float) xGrid * 16.0f;
	float yPos = (float) yGrid * 16.0f;
	clsCoin *pCoin = new clsCoin();
	pCoin->createPicture(d3ddev, L"tileset.png", xPos, yPos, 16, 16);
	pCoin->createFrame(1, 0, 0, 16, 16);
	pCoin->setFrame(0, 3);
	addCoinToCoinList(pCoin);
}

clsCoin* loadCoins(LPDIRECT3DDEVICE9 d3ddev) {

	// On the grass platform
	createCoinObject(3, 29, d3ddev);
	createCoinObject(5, 29, d3ddev);
	createCoinObject(10, 29, d3ddev);

	createCoinObject(15, 29, d3ddev);
	createCoinObject(20, 29, d3ddev);
	createCoinObject(17, 26, d3ddev);
	createCoinObject(18, 26, d3ddev);
	
	createCoinObject(25, 29, d3ddev);
	createCoinObject(30, 29, d3ddev);

	createCoinObject(10, 29, d3ddev);
	createCoinObject(12, 29, d3ddev);
	createCoinObject(15, 29, d3ddev);

	createCoinObject(1, 21, d3ddev);
	createCoinObject(3, 21, d3ddev);
	createCoinObject(5, 21, d3ddev);
	createCoinObject(7, 21, d3ddev);

	createCoinObject(2, 23, d3ddev);
	createCoinObject(4, 23, d3ddev);
	createCoinObject(6, 23, d3ddev);

	createCoinObject(2, 19, d3ddev);
	createCoinObject(4, 19, d3ddev);
	createCoinObject(6, 19, d3ddev);

	// In the cage =Z


	// The big dollar sign
	for (int i = 33; i < 45; i++) {  
		createCoinObject(i, 31, d3ddev); //bottom
		createCoinObject(i, 17, d3ddev); //top
		createCoinObject(i, 24, d3ddev); //middle
	}

	for (int i = 18; i < 24; i++) {
		createCoinObject(45, i + 8, d3ddev); //left
		createCoinObject(33, i, d3ddev); //right
	}

	for (int i = 15; i < 33; i++) {
		createCoinObject(38, i, d3ddev); //the two slashes
		createCoinObject(40, i, d3ddev); //the two slashes
	}
	

	//create a coin offscreen .. a hack for a access violation bug I cannot find
	// has .. something to do with the first element in the list being removed.
	createCoinObject(0, 0, d3ddev);

	return pCoins;
}


void createEnemyObject(int xGrid, int yGrid, int enemyType, LPDIRECT3DDEVICE9 d3ddev)
{

	float xPos = (float) xGrid * 16.0f;
	float yPos = (float) yGrid * 16.0f;

	switch (enemyType) {
	case SPINNER_ENEMY_TYPE:
			clsEnemy *pSpinner01;
			pSpinner01 = new clsSpinner();
			pSpinner01->createPicture(d3ddev, L"spinner.png", xPos, yPos, 16, 16);
			pSpinner01->createFrame(2, 2, 0, 16, 16);
			addEnemyToEnemyList(pSpinner01);

		break;
	case SPIKE_ENEMY_TYPE:
			clsSpike *pSpike01;
			pSpike01 = new clsSpike();
			pSpike01->createPicture(d3ddev, L"spike2.png", xPos, yPos, 16, 16);
			pSpike01->createFrame(1, 1, 0, 16, 16);
			addEnemyToEnemyList(pSpike01);
			break;
	default:
		break;
	}
}


clsDoor* loadDoor(LPDIRECT3DDEVICE9 d3ddev, int xGrid, int yGrid)
{
	float xPos = xGrid * 16.0f;
	float yPos = yGrid * 16.0f;
	clsDoor *pDoor = new clsDoor();
	pDoor->createPicture(d3ddev, L"tileset.png", xPos, yPos, 16, 16);
	pDoor->createFrame(4, 0, 0, 16, 16);
	pDoor->setFrame(0, 2);
	return pDoor;
}

clsEnemy* loadEnemies(LPDIRECT3DDEVICE9 d3ddev) 
{
	
	// Two spinners
	createEnemyObject(12, 12, SPINNER_ENEMY_TYPE, d3ddev);
	createEnemyObject(13, 13, SPINNER_ENEMY_TYPE, d3ddev);
	

	// A floor covered in spikes. Yikes!
	for (int i = 1; i < 49; i++) {
		createEnemyObject(i, 34, SPIKE_ENEMY_TYPE, d3ddev);
	}

	// Spikes at the floor of the grid.
	createEnemyObject(34, 13, SPIKE_ENEMY_TYPE, d3ddev);
	createEnemyObject(35, 13, SPIKE_ENEMY_TYPE, d3ddev);
	createEnemyObject(37, 13, SPIKE_ENEMY_TYPE, d3ddev);
	createEnemyObject(38, 13, SPIKE_ENEMY_TYPE, d3ddev);
	createEnemyObject(40, 13, SPIKE_ENEMY_TYPE, d3ddev);
	createEnemyObject(41, 13, SPIKE_ENEMY_TYPE, d3ddev);
	createEnemyObject(43, 13, SPIKE_ENEMY_TYPE, d3ddev);
	createEnemyObject(44, 13, SPIKE_ENEMY_TYPE, d3ddev);
	createEnemyObject(46, 13, SPIKE_ENEMY_TYPE, d3ddev);
	createEnemyObject(47, 13, SPIKE_ENEMY_TYPE, d3ddev);

	return pEnemy;
}

void addEnemyToEnemyList(clsEnemy *pTemp)
{
	// Add the new enemy to the head of the enemy list
	pTemp->pNext = pEnemy;
	pEnemy = pTemp;
}


void addObjectToTerrainList(clsObject *pTemp)
{
	//Add the new terrain object to the head of the terrain list
	pTemp->pNext = pTerrain;
	pTerrain = pTemp;
}


clsObject* loadLevel(LPDIRECT3DDEVICE9 d3ddev) {
	
	// The border of blue boxes
	for (int i = 0; i < 50; i++) {
		createTerrainObject(i, 35, BLUE_BOX, d3ddev); //bottom
		createTerrainObject(i, 0, BLUE_BOX, d3ddev); //top
	}

	for (int i = 0; i < 36; i++) {
		createTerrainObject(0, i, BLUE_BOX, d3ddev); //left
		createTerrainObject(49, i, BLUE_BOX, d3ddev); //right
	}


    // This is the three grass platforms a ygrid 30 
	createTerrainObject(3, 30, GRASS_LEFT, d3ddev);
	createTerrainObject(4, 30, GRASS_01, d3ddev);
	createTerrainObject(5, 30, GRASS_02, d3ddev);
	createTerrainObject(6, 30, GRASS_03, d3ddev);
	createTerrainObject(7, 30, GRASS_02, d3ddev);
	createTerrainObject(8, 30, GRASS_03, d3ddev);
	createTerrainObject(9, 30, GRASS_02, d3ddev);
	createTerrainObject(10, 30, GRASS_RIGHT, d3ddev);
	
	createTerrainObject(15, 30, GRASS_LEFT, d3ddev);
	createTerrainObject(16, 30, GRASS_01, d3ddev);
	createTerrainObject(17, 30, GRASS_02, d3ddev);
	createTerrainObject(18, 30, GRASS_03, d3ddev);
	createTerrainObject(19, 30, GRASS_02, d3ddev);
	createTerrainObject(20, 30, GRASS_RIGHT, d3ddev);

	createTerrainObject(25, 30, GRASS_LEFT, d3ddev);
	createTerrainObject(26, 30, GRASS_01, d3ddev);
	createTerrainObject(27, 30, GRASS_02, d3ddev);
	createTerrainObject(28, 30, GRASS_03, d3ddev);
	createTerrainObject(29, 30, GRASS_02, d3ddev);
	createTerrainObject(30, 30, GRASS_RIGHT, d3ddev);

	// The grass platform at yGrid 22 (on the left)
	createTerrainObject(1, 22, GRASS_01, d3ddev);
	createTerrainObject(2, 22, GRASS_01, d3ddev);
	createTerrainObject(3, 22, GRASS_02, d3ddev);
	createTerrainObject(4, 22, GRASS_03, d3ddev);
	createTerrainObject(5, 22, GRASS_02, d3ddev);
	createTerrainObject(6, 22, GRASS_01, d3ddev);
	createTerrainObject(7, 22, GRASS_RIGHT, d3ddev);

	// The brick platform under the door
	createTerrainObject(25, 18, BRICK, d3ddev);
	createTerrainObject(26, 18, BRICK, d3ddev);
	createTerrainObject(27, 18, BRICK, d3ddev);


	// The sawtooth platform at ygrid 14
	for (int i = 0; i < 30; i++) {
		if (i % 2) {
			createTerrainObject(i, 14, RED_BOX, d3ddev);
		} else {
			createTerrainObject(i, 14, GOLD_BOX, d3ddev);
			createTerrainObject(i + 1, 13, GREEN_BOX, d3ddev);
		}
	}

	// The other side of the sawtooth platform (floor of the grid on the left)
	for (int i = 33; i < 49; i++) {
		createTerrainObject(i, 14, RED_BOX, d3ddev);
	}

	// Box at 6,3 to 11, 15  (on the left side with missing slot at ygrid 10)
	for (int u = 6; u < 11; u++) {
		for (int v = 3; v < 15; v++) {
			if (!((v >9)&&(v<12))) {
				if ((u%2==1)||(v%2==1))
					createTerrainObject(u, v, RED_BOX, d3ddev);
				else
					createTerrainObject(u, v, BLUE_BOX, d3ddev);
			}
		}
	}

	// The roof that goes from the box to the grid at ygrid 4
	for (int i = 7; i < 33; i++) {
		createTerrainObject(i, 4, RED_BOX, d3ddev);
	}


	// A grid on the left
	for (int u = 32; u < 49; u++) {
		for (int v = 3; v < 14; v++) {
			if (u%3==0) {
				createTerrainObject(u, v, RED_BOX, d3ddev);
			}
		}
	}

	// Create a crap object offscreen
	createTerrainObject(-1, -1, RED_BOX, d3ddev);

	return pTerrain;
}



void createTerrainObject(int xGrid, int yGrid, int terrainType, LPDIRECT3DDEVICE9 d3ddev)
{
	float xPos = (float) xGrid * 16.0f;
	float yPos = (float) yGrid * 16.0f;

	clsObject *pObj = new clsObject();

	pObj->createPicture(d3ddev, L"tileset.png", xPos, yPos, 16, 16);
	pObj->createFrame(1, 0, 0, 16, 16);

	switch (terrainType) {
	case (BLUE_BOX):
		pObj->setFrame(0, 0);
		break;
	case (GREEN_BOX):
		pObj->setFrame(1, 0);
		break;
	case (RED_BOX):
		pObj->setFrame(2, 0);
		break;
	case (GOLD_BOX):
		pObj->setFrame(3, 0);
		break;
	case (BRICK):
		pObj->setFrame(4, 0);
		break;
	case (BOLT):
		pObj->setFrame(5, 0);
		break;
	case (GRASS_LEFT):
		pObj->setFrame(0, 1);
		break;
	case (GRASS_01):
		pObj->setFrame(1, 1);
		break;
	case (GRASS_02):
		pObj->setFrame(2, 1);
		break;
	case (GRASS_03):
		pObj->setFrame(3, 1);
		break;
	case (GRASS_RIGHT):
		pObj->setFrame(4, 1);
		break;
	case (DIRT_01):
		pObj->setFrame(5, 1);
		break;
	case (DIRT_02):
		pObj->setFrame(6, 1);
		break;
	case (DOOR_01):
		pObj->setFrame(0, 2);
		break;
	case (DOOR_02):
		pObj->setFrame(1, 2);
		break;
	case (DOOR_03):
		pObj->setFrame(2, 2);
		break;
	case (DOOR_04):
		pObj->setFrame(3, 2);
		break;
	case (DOOR_05):
		pObj->setFrame(4, 2);
		break;
	case (COIN_01):
		pObj->setFrame(0, 3);
		break;
	case (COIN_02):
		pObj->setFrame(1, 3);
		break;
	case (COIN_03):
		pObj->setFrame(2, 3);
		break;
	case (COIN_04):
		pObj->setFrame(3, 3);
		break;
	default:
		pObj->setFrame(2, 0);
		break;
	}

	addObjectToTerrainList(pObj);
}