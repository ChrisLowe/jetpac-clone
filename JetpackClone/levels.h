/*
 *
 *      CertIV Semester2 2011 C++ Assignment 2   -  Jetpack clone
 *
 *
 *   
 *      Author: Christopher Lowe
 *      Copywrite (C) 2011 Christopher Lowe,  lowey2002@gmail.com
 *      StudentID: 1013775
  *
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * 
 */


#pragma once

#include <d3d9.h>
#include <d3dx9.h>
#define DIRECTINPUT_VERSION 0x0800  //Prevents warning

#include "clsObject.h"
#include "clsEnemy.h"
#include "clsPlayer.h"
#include "clsSpinner.h"
#include "clsSpike.h"
#include "clsCoin.h"
#include "clsDoor.h"


// DirectX library
#pragma comment (lib, "d3d9.lib")
#pragma comment (lib, "d3dx9.lib")


void resetLevelObjects(void);

// Doors
clsDoor* loadDoor(LPDIRECT3DDEVICE9 d3ddev, int xGrid, int yGrid);


// Coins
void createCoinObject(LPDIRECT3DDEVICE9 d3ddev, int xGrid, int yGrid);
clsCoin* loadCoins(LPDIRECT3DDEVICE9 d3ddev);
void addCoinToCoinList(clsCoin *pTemp);
clsCoin* removeCoinFromListOnContact(clsPlayer *pPlayer, IXAudio2* pXA2);


// Enemies
void addEnemyToEnemyList(clsEnemy *pTemp);
clsEnemy* loadEnemies(LPDIRECT3DDEVICE9 d3ddev);
void createEnemyObject(int xGrid, int yGrid, int terrainType, LPDIRECT3DDEVICE9 d3ddev);


// Terrain
clsObject* loadLevel(LPDIRECT3DDEVICE9 d3ddev);
void createTerrainObjectByString(void);
void createTerrainObject(int xGrid, int yGrid, int terrainType, LPDIRECT3DDEVICE9 d3ddev);


// terrainType's
const int SPINNER_ENEMY_TYPE = 1;
const int SPIKE_ENEMY_TYPE = 2;

const int BLUE_BOX = 1;
const int GREEN_BOX = 2;
const int RED_BOX = 3;
const int GOLD_BOX = 4;
const int BRICK = 5;
const int BOLT = 6;
const int UNUSED_01 = 7;

const int GRASS_LEFT = 8;
const int GRASS_01 = 9;
const int GRASS_02 = 10;
const int GRASS_03 = 11;
const int GRASS_RIGHT = 12;
const int DIRT_01 = 13;
const int DIRT_02 = 14;

const int DOOR_01 = 15;
const int DOOR_02 = 16;
const int DOOR_03 = 17;
const int DOOR_04 = 18;
const int DOOR_05 = 19;

const int UNUSED_02 = 20;
const int UNUSED_03 = 21;


const int COIN_01 = 22;
const int COIN_02 = 23;
const int COIN_03 = 24;
const int COIN_04 = 25;
