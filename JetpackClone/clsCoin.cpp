/*
 *
 *      CertIV Semester2 2011 C++ Assignment 2   -  Jetpack clone
 *
 *
 *   
 *      Author: Christopher Lowe
 *      Copywrite (C) 2011 Christopher Lowe,  lowey2002@gmail.com
 *
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * 
 */

#include "StdAfx.h"
#include "clsCoin.h"



clsCoin::clsCoin(void)
{
	pNext = NULL;
	coinSpinDelay = 3.0f;
	coinSpin = 0.0f;

	//srand( time(NULL) );
	int randStartFrame = rand() % 4 - 1;
	spinFrame = randStartFrame;
}


clsCoin::~clsCoin(void)
{
}

void clsCoin::animateSpin(void)
{
	coinSpin++;
	if (coinSpin > coinSpinDelay) {
		coinSpin = 0.0f;

		spinFrame++;
		if (spinFrame > 3) spinFrame = 0;
		setFrame(spinFrame, 3);
	}

}