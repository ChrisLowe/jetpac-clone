/*
 *
 *      CertIV Semester2 2011 C++ Assignment 2   -  Jetpack clone
 *
 *
 *   
 *      Author: Christopher Lowe
 *      Copywrite (C) 2011 Christopher Lowe,  lowey2002@gmail.com
 *
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * 
 */


#pragma once
#include "clsAbstractSprite.h"
#include "clsObject.h"
#include "clsCharacter.h"
#include "playSound.h"


const float MAX_UP_SPEED = 5.0f;  //15.0
const float MIN_UP_SPEED = -2.0f;  //4.0
const float MAX_SIDE_SPEED = 1.5f;  //2.5
const float MIN_SIDE_SPEED = -1.5f;  //-2.5  
	
const float MOVE_SPEED_INCREMENT = 0.05f;
const float FLOAT_SPEED_INCREMENT = 0.003f;
const float COLLISION_FIX_SPEED = 0.05f;

const float COLLECT_SOUND_DELAY = 8.0f;

const float GRAVITY = 9.8f;

class clsPlayer :
	public clsCharacter
{
public:
	clsPlayer(void);
	~clsPlayer(void);
	void moveLeft(void);
	void moveRight(void);
	void moveUp(bool movingLeft);
	void moveDown(void);
	void cannotMoveUp(bool movingLeft);
	void setMoveSpeed(float newMoveSpeed);
	void startThrust(bool movingLeft);
	void endThrust(bool movingLeft);
	void animateWalk(bool movingLeft);
	void animateThrust(bool movingLeft);
	void die(void);

	void setCollectSound(IXAudio2 *pXAudio2, LPWSTR filename);
	void playCollectSound(IXAudio2 *pXAudio2);



	// Members
	bool isDead;
	float vUp;
	float vSide;

private:
	bool thrustOn;
	int playerMoveDelay;
	float playerMoveSpeed;
	float collectSound;

	IXAudio2 *pXA2;
	WAVEFORMATEXTENSIBLE wfx;

	//XAUDIO2_BUFFER buffer;
	XAUDIO2_BUFFER collectBuffer;
	XAUDIO2_BUFFER thrustBuffer;
	IXAudio2SourceVoice* pSourceVoice;

};

