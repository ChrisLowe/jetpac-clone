/*
 *
 *      CertIV Semester2 2011 C++ Assignment 2   -  Jetpack clone
 *
 *
 *   
 *      Author: Christopher Lowe
 *      Copywrite (C) 2011 Christopher Lowe,  lowey2002@gmail.com
 *
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * 
 */

#include "StdAfx.h"
#include "clsCharacter.h"
#include "util.h"


clsCharacter::clsCharacter(void)
{
}


clsCharacter::~clsCharacter(void)
{
}


bool clsCharacter::simpleCollision(clsObject *pObj) {

	clsObject *pTemp = pObj;


	float objX = pTemp->getXPos();
	float objY = pTemp->getYPos();
	float objHeight = pTemp->getHeight();
	float objWidth = pTemp->getWidth();

	if (rectInRect(xPos, objX, xPos + width, objX + objWidth, yPos, objY, yPos + width, objY + objHeight)) {
		return true;
	}
	
	

	return false;
}




collision_t* clsCharacter::collision(clsObject *pObj)
{
	clsObject *pTemp = pObj;

	collision_t *col = new collision_t();
	
	while (pTemp != NULL)
	{
		float objX = pTemp->getXPos();
		float objY = pTemp->getYPos();
		float objHeight = pTemp->getHeight();
		float objWidth = pTemp->getWidth();

		// Direct hit collision
		if ((objY <= yPos + height - 2) && (objY >= yPos + 8) &&
				(objX <= xPos + width - 8) && (objX >= xPos + 8)) {
				//changeLocation(xPos, yPos - 0.2f);
		}

		if (rectInRect(xPos, objX, xPos + width, objX + objWidth, yPos, objY, yPos + height, objY + height)) {

			// I call these point rays. (In 3D you would cast a ray, here in 2D I cast a point)
			// I fire three points in various directions and test if the point is inside an clsObject
			//
			//                 .
			//                 |
			//           .__ |----| __.
			//           .__ |    | __.
			//           .__ |src | __.
			//				 |----| 
			//                 ||
			//                 ..
			//
 			if (pointInRect(objX, objY, objX + objWidth, objY + objHeight, xPos + (0.75f * width), yPos + 2.0f))
				col->right = true;
			if (pointInRect(objX, objY, objX + objWidth, objY + objHeight, xPos + (0.75f * width), yPos + (0.5f * height + 2.0f)))
				col->right = true;
			if (pointInRect(objX, objY, objX + objWidth, objY + objHeight, xPos + (0.75f * width), yPos + (0.75f * height + 2.0f)))
				col->right = true;

			if (pointInRect(objX, objY, objX + objWidth, objY + objHeight, xPos + (0.25f * width), yPos + 2.0f))
				col->left = true;
			if (pointInRect(objX, objY, objX + objWidth, objY + objHeight, xPos + (0.25f * width), yPos + (0.5f * height + 2.0f)))
				col->left = true;
			if (pointInRect(objX, objY, objX + objWidth, objY + objHeight, xPos + (0.25f * width), yPos + (0.75f * height) + 2.0f))
				col->left = true;
				

			if (pointInRect(objX, objY, objX + objWidth, objY + objHeight, xPos + (0.35f * width), yPos + height))
				col->down = true;
			if (pointInRect(objX, objY, objX + objWidth, objY + objHeight, xPos + (0.65f * width), yPos + height))
				col->down = true;

			
			if (pointInRect(objX, objY, objX + objWidth, objY + objHeight, xPos + (0.5f * width), yPos))
 				col->up = true;
		}

		pTemp = pTemp->pNext;

	}

	return col;
}



int clsCharacter::directCollision(clsObject *pObj) {
	clsObject *pTemp = pObj;

	while (pTemp != NULL)
	{
		float objX = pTemp->getXPos();
		float objY = pTemp->getYPos();
		float objHeight = pTemp->getHeight();
		float objWidth = pTemp->getWidth();

		// Direct hit collision
		if ((objY <= yPos + height - 2) && (objY >= yPos + 8) &&
				(objX <= xPos + width - 8) && (objX >= xPos + 8)) {
				changeLocation(xPos, yPos - 0.2f);
		}

		if (rectInRect(xPos, objX, xPos + width, objX + objWidth, yPos, objY, yPos + height, objY + height)) {

			if (pointInRect(objX, objY, objX + objWidth, objY + objHeight, xPos + (0.75f * width), yPos + (0.5f * height) + 2.0f))
				return RIGHT_COLLISION;

			else if (pointInRect(objX, objY, objX + objWidth, objY + objHeight, xPos - (0.25f * width), yPos + (0.5f * height) + 2.0f))
				return LEFT_COLLISION;

			else if (pointInRect(objX, objY, objX + objWidth, objY + objHeight, xPos + (0.5f * width), yPos + height))  //+2.0f
				return FLOOR_COLLISION;
			
			else if (pointInRect(objX, objY, objX + objWidth, objY + objHeight, xPos + (0.5f * width), yPos))
				return ROOF_COLLISION;

		}

		pTemp = pTemp->pNext;

	}

	return 0;
}
