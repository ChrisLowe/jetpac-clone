/*
 *
 *      CertIV Semester2 2011 C++ Assignment 2   -  Jetpack clone
 *
 *
 *   
 *      Author: Christopher Lowe
 *      Copywrite (C) 2011 Christopher Lowe,  lowey2002@gmail.com
 * 
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * 
 */

#include "StdAfx.h"
#include "util.h"


// Returns true if rectangle l in inside 2
bool rectInRect(float left1, float left2, float right1, float right2, float top1, float top2, float bottom1, float bottom2)
{
	if (bottom1 < top2) return false;
	if (top1 > bottom2) return false;
	if (right1 < left2) return false;
	if (left1 > right2) return false;

	return true;
}
	
// Returns true if the point is in the rectangle
bool pointInRect(float topX, float leftY, float bottomX, float rightY, float x, float y)
{
	if ((x >= topX) && (x < bottomX) && (y >= leftY) && (y < rightY)) return true;
	return false;
}


// Returns true if the line is inside the rectangle
bool lineInRect(float lineStartX, float lineStartY, float lineEndX, float lineEndY, float leftX, float topY, float rightX, float bottomY)
{

	if (((lineStartX > leftX) && (lineStartX < rightX) && (lineStartY > bottomY) && (lineStartY < topY)) ||
		((lineEndX > leftX) && (lineEndX < rightX) && (lineEndY > bottomY) && (lineEndY < topY))) {
			return true;
	}
	return false;
}



